import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Rental } from './rental';
import { RentalService } from './rental.service';

describe('RentalService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let rentalService: RentalService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [RentalService]
        });

        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
        rentalService = TestBed.get(RentalService);
    });


    it('should be created', inject([RentalService], (service: RentalService) => {
        //then
        expect(service).toBeTruthy();
    }));

    describe('#getRentals', () => {
        let expectedRentals: Rental[];

        beforeEach(() => {
            rentalService = TestBed.get(RentalService);
            expectedRentals = [
                {
                    id: 1,
                    customerId: 1,
                    carId: 2,
                    dateFrom: new Date(2018, 3, 6),
                    dateTo: new Date(2018, 3, 9),
                    hasWifi: false,
                    hasGPS: true,
                    hasChildSeat: false,
                    isAccepted: true
                },
                {
                    id: 2,
                    customerId: 3,
                    carId: 6,
                    dateFrom: new Date(2018, 3, 8),
                    dateTo: new Date(2018, 3, 12),
                    hasWifi: true,
                    hasGPS: true,
                    hasChildSeat: false,
                    isAccepted: true
                }
            ] as Rental[];
        });

        it('should return expected rentals', () => {
            //when
            rentalService.getRentals().subscribe(
                rentals => expect(rentals).toEqual(expectedRentals, 'should return expected rentals'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(rentalService.rentalsUrl);
            expect(req.request.method).toEqual('GET');
        });



        it('should return expected rentals when function called multiple times', () => {
            //when
            rentalService.getRentals().subscribe();
            rentalService.getRentals().subscribe();
            rentalService.getRentals().subscribe(
                rentals => expect(rentals).toEqual(expectedRentals, 'should return expected rentals'),
                fail
            );

            //then
            const requests = httpTestingController.match(rentalService.rentalsUrl);
            expect(requests.length).toEqual(3, 'calls to getRentals()');

        });
    });

    describe('#updateRental', () => {

        it('should update a rental and return it', () => {
            //given
            const updateRental: Rental = {
                id: 1,
                customerId: 2,
                carId: 7,
                dateFrom: new Date(),
                dateTo: new Date(),
                hasWifi: false,
                hasGPS: false,
                hasChildSeat: false,
                isAccepted: false
            };

            //when
            rentalService.updateRental(updateRental).subscribe(
                data => expect(data).toEqual(updateRental, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(rentalService.rentalsUrl);
            expect(req.request.method).toEqual('PUT');
            expect(req.request.body).toEqual(updateRental);

        });

    });

    describe('#addRental', () => {

        it('should add a rental and return it', () => {
            //given
            const newRental: Rental = {
                id: 1,
                customerId: 2,
                carId: 7,
                dateFrom: new Date(),
                dateTo: new Date(),
                hasWifi: false,
                hasGPS: false,
                hasChildSeat: false,
                isAccepted: false
            };

            //when
            rentalService.addRental(newRental).subscribe(
                data => expect(data).toEqual(newRental, 'should return the rental'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(rentalService.rentalsUrl);
            expect(req.request.method).toEqual('POST');
            expect(req.request.body).toEqual(newRental);

        });

    });

    describe('#getRental', () => {

        it('should return expected rental (called once)', () => {
            //given
            const newRental: Rental = {
                id: 1,
                customerId: 2,
                carId: 7,
                dateFrom: new Date(),
                dateTo: new Date(),
                hasWifi: false,
                hasGPS: false,
                hasChildSeat: false,
                isAccepted: false
            };
            rentalService.addRental(newRental);

            //when
            rentalService.getRental(1).subscribe(
                data => expect(data).toEqual(newRental, 'should return the rental'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(rentalService.rentalsUrl + '/1');
            expect(req.request.method).toEqual('GET');

        });
    });

    describe('#deleteRental', () => {

        it('should delete expected rental (called once)', () => {
            //given
            const newRental: Rental = {
                id: 1,
                customerId: 2,
                carId: 7,
                dateFrom: new Date(),
                dateTo: new Date(),
                hasWifi: false,
                hasGPS: false,
                hasChildSeat: false,
                isAccepted: false
            };
            rentalService.addRental(newRental);

            //when
            rentalService.deleteRental(1).subscribe(
                data => expect(data).toEqual(newRental, 'should return the rental'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(rentalService.rentalsUrl + '/1');
            expect(req.request.method).toEqual('DELETE');
            expect(rentalService.getRental(1)).toBeUndefined;

        });
    });

    describe('#checkValidators', () => {

        let existingRentals: Rental[];

        beforeEach(() => {
            spyOn(window, 'alert');
            existingRentals = [
                {
                    id: 1,
                    customerId: 2,
                    carId: 7,
                    dateFrom: new Date(2018, 5, 6),
                    dateTo: new Date(2018, 5, 10),
                    hasWifi: false,
                    hasGPS: false,
                    hasChildSeat: false,
                    isAccepted: false
                },
                {
                    id: 2,
                    customerId: 3,
                    carId: 9,
                    dateFrom: new Date(2018, 7, 9),
                    dateTo: new Date(2018, 7, 16),
                    hasWifi: false,
                    hasGPS: false,
                    hasChildSeat: false,
                    isAccepted: false
                },
            ] as Rental[];
        });

        it('should validate date range when dateFrom before dateTo', () => {
            //given
            let dateTo = new Date(2018, 6, 12),
                dateFrom = new Date(2018, 6, 5);

            //then
            expect(rentalService.validateRange(dateTo, dateFrom)).toBe(true);
        });

        it('should validate date range when dateTo before dateFrom', () => {
            //given
            let dateTo = new Date(2018, 6, 12),
                dateFrom = new Date(2018, 6, 23);

            //then
            expect(rentalService.validateRange(dateTo, dateFrom)).toBe(false);
            expect(window.alert).toHaveBeenCalledWith('Date from must be before date to');
        });

        it('should validate date when date range not in service', () => {
            //given
            let dateTo = new Date(2018, 6, 12),
                dateFrom = new Date(2018, 6, 5),
                carServicePeriod = new Date(2018, 6, 13);

            //then
            expect(rentalService.validateServiceDate(dateTo, dateFrom, carServicePeriod)).toBe(true);
        });

        it('should validate date range when date range during service', () => {
            //given
            let dateTo = new Date(2018, 6, 26),
                dateFrom = new Date(2018, 6, 23),
                carServicePeriod = new Date(2018, 6, 25);

            //then
            expect(rentalService.validateServiceDate(dateTo, dateFrom, carServicePeriod)).toBe(false);
            expect(window.alert).toHaveBeenCalledWith('Choose another period, car is in service');
        });

        it('should validate date when date range when car available', () => {
            //given
            let dateTo = new Date(2018, 5, 15),
                dateFrom = new Date(2018, 5, 11),
                carId = 7;

            //then
            expect(rentalService.validateReservedDates(existingRentals, carId, dateTo, dateFrom)).toBe(true);
        });

        it('should validate date range when date range car already rent', () => {
            //given
            let dateTo = new Date(2018, 5, 11),
                dateFrom = new Date(2018, 5, 7),
                carId = 7;

            //then
            expect(rentalService.validateReservedDates(existingRentals, carId, dateTo, dateFrom)).toBe(false);
            expect(window.alert).toHaveBeenCalledWith('Car already reserved in this period');
        });


    });


});