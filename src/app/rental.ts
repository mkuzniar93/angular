export class Rental {
    id: number;
    customerId: number;
    carId: number;
    dateFrom: Date;
    dateTo: Date;
    hasWifi: boolean;
    hasGPS: boolean;
    hasChildSeat: boolean;
    isAccepted: boolean;
  }