import { Injectable } from '@angular/core';

@Injectable()
export class AdministratorService {

  private isAdministratorLoggedIn;

  constructor() { 
    this.isAdministratorLoggedIn = false;
  }

  setAdministratorLoggedIn(){
    this.isAdministratorLoggedIn = true;
  }

  getAdministratorLoggedIn(){
    return this.isAdministratorLoggedIn;
  }

}
