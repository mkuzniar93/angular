import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer';
import { AdministratorService } from '../administrator.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  customers: Customer[];

  constructor(private router: Router, private customer: CustomerService, private administrator: AdministratorService) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.customer.getCustomers().subscribe(customers => this.customers = customers);
  }

  loginUser(username: string, password: string) {
  	
  	if(username == 'admin' && password == 'admin') {
      this.administrator.setAdministratorLoggedIn();
  		this.router.navigate(['dashboard']);
  	} else if(this.checkExistingUser(username, password)){
      this.customer.setCustomerLoggedIn();
      this.router.navigate(['customer-dashboard']);
    } else {
      alert("Invalid credentials");
    }
  }

  checkExistingUser(username: string, password: string){
    for(let i = 0; i < this.customers.length; i++){
      if(this.customers[i].login === username && this.customers[i].password === password){
        this.customer.setLoggedCustomerId(this.customers[i].id);
        return true;
      }
    }
    return false;
  }

}
