import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginFormComponent } from './login-form.component';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { AdministratorService } from '../administrator.service';
import { Customer } from '../customer';
import { defer } from 'rxjs/observable/defer'
import { of } from 'rxjs/observable/of'

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const customerServiceSpy = jasmine.createSpyObj('CustomerService', ['getCustomers', 'setLoggedCustomerId', 'setCustomerLoggedIn']) 
  const adminServiceSpy = jasmine.createSpyObj('AdministratorService', ['setAdministratorLoggedIn']);
  const customers = [
    {
      id: 1,
      name: 'Antek',
      surname: 'Smykiewicz',
      email: 'asmykiewicz@gmail.com',
      login: 'customer1',
      password: 'customer1',
      DOB: new Date(1991, 5, 21),
      rentCars: [2]
    },
    {
      id: 2,
      name: 'Janusz',
      surname: 'Tracz',
      email: 'jtrancz@gmail.com',
      login: 'customer2',
      password: 'customer2',
      DOB: new Date(1987, 3, 14),
      rentCars: []
    },
    ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [ LoginFormComponent ],
      providers: [
          {provide: Router, useValue: routerSpy},
          {provide: CustomerService, useValue: customerServiceSpy},
          {provide: AdministratorService, useValue: adminServiceSpy}
      ]
    })
    .compileComponents();

    customerServiceSpy.getCustomers.and.returnValue(asyncData(of(customers)));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(window, 'alert');

  });

  it('should create', () => {
    //then
    expect(component).toBeTruthy();
  });

  it('should return true when customer exists', () => {
    //given
    component.customers = customers;
    let username = 'customer1',
    password = 'customer1';
    //then
    expect(component.checkExistingUser(username, password)).toBeTruthy();
  });

  it('should return true when customer does not exist', () => {
    //given
    component.customers = customers;
    let username = 'customer6',
    password = 'customer6';
    //then
    expect(component.checkExistingUser(username, password)).toBeFalsy();
  });

  it('should throw alert when invalid credentials', () => {
    //given
    component.customers = customers;
    let username = 'asda',
    password = 'asda';
    //when
    component.loginUser(username, password);
    //then
    expect(window.alert).toHaveBeenCalledWith('Invalid credentials');
  });

  it('should navigate to dashboard when admin logged in', () => {
    //given
    component.customers = customers;
    let username = 'admin',
    password = 'admin';
    //when
    component.loginUser(username, password);
    //then
    expect(routerSpy.navigate).toHaveBeenCalledWith(['dashboard']);
  });

  it('should navigate to customer-dashboard when customer logged in', () => {
    //given
    component.customers = customers;
    let username = 'customer1',
    password = 'customer1';
    //when
    component.loginUser(username, password);
    //then
    expect(routerSpy.navigate).toHaveBeenCalledWith(['customer-dashboard']);
  });

  function asyncData<T>(data: T) {
     return defer(() => Promise.resolve(data));
} 
});


