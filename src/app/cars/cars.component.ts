import { Component, OnInit, TemplateRef } from '@angular/core';

import { Car } from '../car';
import { CarService } from '../car.service';
import { element } from 'protractor';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  cars: Car[];
  path: string[] = ['car'];
  order: number = 1;
  searchString: string;
  modalRef: BsModalRef;

  constructor(private carService: CarService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  decline(): void {
    this.modalRef.hide();
  }

  add(model: string, mark: string, description: string, remarks: string, color: string, productionDate: Date, retirementDate: Date, mileage: number, servicePeriod: Date): void {
    if (!model || !mark || !productionDate || !retirementDate || !mileage || !servicePeriod) { 
      alert("Necessary fields have to be filled");
      return; }
    this.carService.addCar({ model, mark, description, remarks, color, productionDate, retirementDate, mileage, servicePeriod } as Car)
      .subscribe(car => {
        this.cars.push(car);
      });
      alert("Car added!");
  }

  delete(car: Car): void {
    this.cars = this.cars.filter(c => c !== car);
    this.carService.deleteCar(car).subscribe();
    this.modalRef.hide();
  }

  sortTable(prop: string) {
    this.path = prop.split('.')
    this.order = this.order * (-1);
    return false;
  }

}
