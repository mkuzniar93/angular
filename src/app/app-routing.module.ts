import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CarsComponent }      from './cars/cars.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { CarDetailComponent }  from './car-detail/car-detail.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AuthguardGuard } from './authguard.guard';
import { Authguard2Guard } from './authguard2.guard';
import { CustomersComponent } from './customers/customers.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { ReservationComponent } from './reservation/reservation.component';
import { RentalsComponent } from './rentals/rentals.component';
import { RentalDetailComponent } from './rental-detail/rental-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginFormComponent },
  { path: 'customer-dashboard', component: CustomerDashboardComponent, canActivate: [AuthguardGuard] },
  { path: 'reservation/:id', component: ReservationComponent, canActivate: [AuthguardGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [Authguard2Guard] },
  { path: 'cars', component: CarsComponent, canActivate: [Authguard2Guard] },
  { path: 'cardetail/:id', component: CarDetailComponent, canActivate: [Authguard2Guard] },
  { path: 'customers', component: CustomersComponent, canActivate: [Authguard2Guard] },
  { path: 'customerdetail/:id', component: CustomerDetailComponent, canActivate: [Authguard2Guard] },
  { path: 'rentals', component: RentalsComponent, canActivate: [Authguard2Guard] },
  { path: 'rentaldetail/:id', component: RentalDetailComponent, canActivate: [Authguard2Guard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
