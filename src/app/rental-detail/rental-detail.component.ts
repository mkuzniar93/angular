import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Rental } from '../rental';
import { RentalService }  from '../rental.service';

@Component({
  selector: 'app-rental-detail',
  templateUrl: './rental-detail.component.html',
  styleUrls: ['./rental-detail.component.css']
})
export class RentalDetailComponent implements OnInit {

  @Input() rental: Rental;

  constructor(
    private route: ActivatedRoute,
    private rentalService: RentalService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getRental();
  }

  getRental(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.rentalService.getRental(id)
      .subscribe(rental => this.rental = rental);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.rentalService.updateRental(this.rental)
      .subscribe(() => this.goBack());
  }

}
