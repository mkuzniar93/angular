import { Pipe, PipeTransform } from '@angular/core';
import { Customer } from './customer';

@Pipe({
  name: 'sortingCustomers'
})
export class SortingCustomersPipe implements PipeTransform {

  transform(customers: Customer[], path: string[], order: number): Customer[] {
    if (!customers || !path || !order) return customers;

    return customers.sort((a: Customer, b: Customer) => {
      path.forEach(property => {
        a = a[property];
        b = b[property];
      })

      return a > b ? order : order * (- 1);
    })
  }

}
