import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { Rental } from './rental';
import { CustomerService } from './customer.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RentalService {

  rentalsUrl = 'api/rentals';

  constructor(
    private http: HttpClient) { }

  getRentals(): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl);
  }

  getRental(id: number): Observable<Rental> {
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.get<Rental>(url);
  }

  updateRental(rental: Rental): Observable<any> {
    return this.http.put(this.rentalsUrl, rental, httpOptions);
  }

  addRental(rental: Rental): Observable<Rental> {
    return this.http.post<Rental>(this.rentalsUrl, rental, httpOptions);
  }

  validateRange(dateTo, dateFrom): boolean {
    if(dateTo < dateFrom){
      alert("Date from must be before date to");
      return false;
    }
    return true;
  }

  validateServiceDate(dateTo, dateFrom, carServicePeriod): boolean {
    if(dateFrom <= carServicePeriod && dateTo >= carServicePeriod){
    alert("Choose another period, car is in service");
    return false;
    }
    return true;
  }

  validateReservedDates(rentals, carId, dateTo, dateFrom): boolean {
    for(let i = 0; i < rentals.length; i++){
      if(rentals[i].carId === carId){
        let rentalDateFrom = new Date(rentals[i].dateFrom),
        rentalDateTo = new Date(rentals[i].dateTo);
        if (!(dateTo < rentalDateFrom) && !(dateFrom > rentalDateTo)) {
        alert("Car already reserved in this period");
        return false;
        }
      }
    }
    return true;
  }

  deleteRental(rental: Rental | number): Observable<Rental> {
    const id = typeof rental === 'number' ? rental : rental.id;
    const url = `${this.rentalsUrl}/${id}`;

    return this.http.delete<Rental>(url, httpOptions);
  }

}
