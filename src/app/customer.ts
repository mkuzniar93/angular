export class Customer {
    id: number;
    name: string;
    surname: string;
    email: string;
    login: string;
    password: string;
    DOB: Date;
    rentCars: number[];
  }