import { Component, OnInit, TemplateRef } from '@angular/core';

import { Rental } from '../rental';
import { RentalService } from '../rental.service';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import { element } from 'protractor';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Car } from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.css']
})
export class RentalsComponent implements OnInit {

  rentals: Rental[];
  customers: Customer[];
  cars: Car[];
  path: string[] = ['car'];
  order: number = 1;
  searchString: string;
  modalRef: BsModalRef;
  checkHasWifi: boolean = false;
  checkHasGPS: boolean = false;
  checkHasChildSeat: boolean = false;
  checkIsAccepted: boolean = false;

  constructor(private rentalService: RentalService, private customerService: CustomerService, private carService: CarService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getRentals();
    this.getCustomers();
    this.getCars();
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  decline(): void {
    this.modalRef.hide();
  }

  add(customerId: number, carId: number, dateFrom: Date, dateTo: Date): void {
    if (carId) {
      let hasWifi = this.checkHasWifi,
        hasGPS = this.checkHasGPS,
        hasChildSeat = this.checkHasChildSeat,
        isAccepted = this.checkIsAccepted,
        newDateFrom = new Date(dateFrom),
        newDateTo = new Date(dateTo),
        newCarId = Number(carId),
        carServicePeriod = new Date(this.cars[carId - 1].servicePeriod);
      if (this.rentalService.validateRange(dateTo, dateFrom)
        && this.rentalService.validateServiceDate(newDateTo, newDateFrom, carServicePeriod)
        && this.rentalService.validateReservedDates(this.rentals, newCarId, newDateTo, newDateFrom)) {
        if (!customerId || !carId) {
          alert("Customer id and car id are necessary");
          return;
        }
        this.rentalService.addRental({ customerId, carId, dateFrom, dateTo, hasWifi, hasGPS, hasChildSeat, isAccepted } as Rental)
          .subscribe(rental => {
            this.rentals.push(rental);
          });
        this.customers[customerId - 1].rentCars.push(carId);
        this.customerService.updateCustomer(this.customers[customerId - 1])
          .subscribe();
        alert("Rental succesfully added");
        this.resetCheckbox();
      }
    }
  }

  resetCheckbox(): void {
    this.checkHasWifi = false;
    this.checkHasGPS = false;
    this.checkHasChildSeat = false;
    this.checkIsAccepted = false;
  }

  rentCar(rental: Rental) {
    rental.isAccepted = true;
    rental.hasWifi = this.checkHasWifi;
    rental.hasGPS = this.checkHasGPS;
    rental.hasChildSeat = this.checkHasChildSeat;
    this.rentalService.updateRental(rental)
      .subscribe();
    this.modalRef.hide();
  }

  suspendRental(rental: Rental) {
    rental.isAccepted = false;
    rental.dateFrom = new Date(0);
    rental.dateTo = new Date(0);
    this.rentalService.updateRental(rental)
      .subscribe();
  }

  delete(rental: Rental): void {
    this.rentals = this.rentals.filter(r => r !== rental);
    this.rentalService.deleteRental(rental).subscribe();
    let index = this.customers[rental.customerId - 1].rentCars.indexOf(rental.carId, 0);
    if (index > -1) {
      this.customers[rental.customerId - 1].rentCars.splice(index, 1);
    }
    this.customerService.updateCustomer(this.customers[rental.customerId - 1])
      .subscribe();
    this.modalRef.hide();
  }

  sortTable(prop: string) {
    this.path = prop.split('.')
    this.order = this.order * (-1);
    return false;
  }

}
