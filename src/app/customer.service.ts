import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Customer } from './customer';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CustomerService {

  private customersUrl = 'api/customers';

  private isCustomerLoggedIn;
  private loggedCustomerId;

  constructor(
    private http: HttpClient
  ) {
    this.isCustomerLoggedIn = false;
  }

  setCustomerLoggedIn() {
    this.isCustomerLoggedIn = true;
  }

  getCustomerLoggedIn() {
    return this.isCustomerLoggedIn;
  }

  setLoggedCustomerId(customerId: number) {
    this.loggedCustomerId = customerId;
  }

  getLoggedCustomerId() {
    return this.loggedCustomerId;
  }

  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.customersUrl);
  }

  getCustomer(id: number): Observable<Customer> {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url);
  }

  updateCustomer(customer: Customer): Observable<any> {
    return this.http.put(this.customersUrl, customer, httpOptions);
  }

  addCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(this.customersUrl, customer, httpOptions);
  }

  deleteCustomer(customer: Customer | number): Observable<Customer> {
    const id = typeof customer === 'number' ? customer : customer.id;
    const url = `${this.customersUrl}/${id}`;

    return this.http.delete<Customer>(url, httpOptions);
  }

  searchCustomers(term: string): Observable<Customer[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Customer[]>(`api/customers/?surname=${term}`);
  }

}
