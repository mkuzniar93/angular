import { Pipe, PipeTransform } from '@angular/core';
import { Car } from './car';

@Pipe({
  name: 'sortingCars'
})
export class SortingCarsPipe implements PipeTransform {

  transform(cars: Car[], path: string[], order: number): Car[] {
    if (!cars || !path || !order) return cars;

    return cars.sort((a: Car, b: Car) => {
      path.forEach(property => {
        a = a[property];
        b = b[property];
      })

      return a > b ? order : order * (- 1);
    })
  }

}
