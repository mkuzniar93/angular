import { Component, OnInit, TemplateRef } from '@angular/core';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';
import { element } from 'protractor';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers: Customer[];
  path: string[] = ['customer'];
  order: number = 1;
  maxDate: Date;
  searchString: string;
  modalRef: BsModalRef;

  constructor(private customerService: CustomerService, private modalService: BsModalService) { 
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - 1);
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  decline(): void {
    this.modalRef.hide();
  }

  add(name: string, surname: string, email: string, login: string, password: string, DOB: Date): void {
    let rentCars = [];
    name = name.trim();
    if (!name || !surname || !email || !login || !password || !DOB) { 
      alert("All fields are necessary");
      return; }
    this.customerService.addCustomer({ name, surname, email, login, password, DOB, rentCars } as Customer)
      .subscribe(customer => {
        this.customers.push(customer);
      });
    alert("Customer added!");
  }

  delete(customer: Customer): void {
    this.customers = this.customers.filter(c => c !== customer);
    this.customerService.deleteCustomer(customer).subscribe();
    this.modalRef.hide();
  }

  sortTable(prop: string) {
    this.path = prop.split('.')
    this.order = this.order * (-1);
    return false;
  }

}
