import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Car } from './car';
import { CarService } from './car.service';

describe('CarService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let carService: CarService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CarService]
        });

        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
        carService = TestBed.get(CarService);
    });


    it('should be created', inject([CarService], (service: CarService) => {
        //then
        expect(service).toBeTruthy();
    }));

    describe('#getCars', () => {
        let expectedCars: Car[];

        beforeEach(() => {
            carService = TestBed.get(CarService);
            expectedCars = [
                {
                    id: 3,
                    mark: 'Alfa Romeo',
                    model: 'Giulietta',
                    description: 'Compact car',
                    color: 'white',
                    productionDate: new Date(2013, 4, 15),
                    retirementDate: new Date(2023, 4, 15),
                    mileage: 100412,
                    servicePeriod: new Date(2018, 6, 10)
                },
                {
                    id: 4,
                    mark: 'Renault',
                    model: 'Captur',
                    description: 'Crossover',
                    color: 'gray',
                    productionDate: new Date(2015, 5, 10),
                    retirementDate: new Date(2025, 5, 10),
                    mileage: 74865,
                    servicePeriod: new Date(2018, 7, 14)
                }
            ] as Car[];
        });

        it('should return expected cars', () => {
            //when
            carService.getCars().subscribe(
                cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl);
            expect(req.request.method).toEqual('GET');

        });


        it('should return expected cars when function called multiple times', () => {
            //when
            carService.getCars().subscribe();
            carService.getCars().subscribe();
            carService.getCars().subscribe(
                cars => expect(cars).toEqual(expectedCars, 'should return expected cars'),
                fail
            );

            //then
            const requests = httpTestingController.match(carService.carsUrl);
            expect(requests.length).toEqual(3, 'calls to getCars()');

        });
    });

    describe('#updateCar', () => {

        it('should update a car and return it', () => {
            //given
            const updateCar: Car = {
                id: 1,
                mark: 'Renault',
                model: 'Clio',
                description: 'Compact car',
                remarks: '',
                color: 'red',
                productionDate: new Date(2010, 9, 12),
                retirementDate: new Date(2020, 9, 12),
                mileage: 1234356,
                servicePeriod: new Date(2018, 4, 4)
            };

            //when
            carService.updateCar(updateCar).subscribe(
                data => expect(data).toEqual(updateCar, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl);
            expect(req.request.method).toEqual('PUT');
            expect(req.request.body).toEqual(updateCar);

        });

    });

    describe('#addCar', () => {

        it('should add a car and return it', () => {
            //given
            const newCar: Car = {
                id: 1,
                mark: 'Renault',
                model: 'Clio',
                description: 'Compact car',
                remarks: '',
                color: 'red',
                productionDate: new Date(2010, 9, 12),
                retirementDate: new Date(2020, 9, 12),
                mileage: 1234356,
                servicePeriod: new Date(2018, 4, 4)
            };

            //when
            carService.addCar(newCar).subscribe(
                data => expect(data).toEqual(newCar, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl);
            expect(req.request.method).toEqual('POST');
            expect(req.request.body).toEqual(newCar);

        });

    });

    describe('#searchCar', () => {

        it('should find a car by model and return it', () => {
            //given
            let expectedCar: Car[],
                model = "m0";

            expectedCar = [{
                id: 1,
                mark: 'Renault',
                model: 'Clio',
                description: 'Compact car',
                remarks: '',
                color: 'red',
                productionDate: new Date(2010, 9, 12),
                retirementDate: new Date(2020, 9, 12),
                mileage: 1234356,
                servicePeriod: new Date(2018, 4, 4)
            }];

            //when
            carService.searchCars(model).subscribe(
                cars => expect(cars).toEqual(expectedCar, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl + "/?model=m0");
            expect(req.request.method).toEqual('GET');

        });

    });

    describe('#getCar', () => {

        it('should return expected car', () => {
            //given
            const newCar: Car = {
                id: 1,
                mark: 'Renault',
                model: 'Clio',
                description: 'Compact car',
                remarks: '',
                color: 'red',
                productionDate: new Date(2010, 9, 12),
                retirementDate: new Date(2020, 9, 12),
                mileage: 1234356,
                servicePeriod: new Date(2018, 4, 4)
            };

            //when
            carService.addCar(newCar);
            carService.getCar(1).subscribe(
                data => expect(data).toEqual(newCar, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl + '/1');
            expect(req.request.method).toEqual('GET');

        });
    });

    describe('#deleteCar', () => {

        it('should delete expected car (called once)', () => {
            //given
            const newCar: Car = {
                id: 1,
                mark: 'Renault',
                model: 'Clio',
                description: 'Compact car',
                remarks: '',
                color: 'red',
                productionDate: new Date(2010, 9, 12),
                retirementDate: new Date(2020, 9, 12),
                mileage: 1234356,
                servicePeriod: new Date(2018, 4, 4)
            };
            carService.addCar(newCar);

            //when
            carService.deleteCar(1).subscribe(
                data => expect(data).toEqual(newCar, 'should return the car'),
                fail
            );

            //then
            const req = httpTestingController.expectOne(carService.carsUrl + '/1');
            expect(req.request.method).toEqual('DELETE');
            expect(carService.getCar(1)).toBeUndefined;

        });
    });

});