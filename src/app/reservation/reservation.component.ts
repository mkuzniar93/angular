import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Car } from '../car';
import { CarService } from '../car.service';
import { Rental } from '../rental';
import { RentalService } from '../rental.service';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  car: Car;
  minDate: Date;
  dateFrom: Date;
  dateTo: Date;
  rentals: Rental[];
  customers: Customer[];

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
    private location: Location,
    private rentalService: RentalService,
    private customerService: CustomerService
  ) {
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
  }

  ngOnInit(): void {
    this.getCar();
    this.getCustomers();
    this.getRentals();
  }

  getCar(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.carService.getCar(id)
      .subscribe(car => this.car = car);
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => this.customers = customers);
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  goBack(): void {
    this.location.back();
  }

  setDateFrom(value: Date): void {
    this.dateFrom = value;
  }

  getDateFrom(): Date {
    return this.dateFrom;
  }

  setDateTo(value: Date): void {
    this.dateTo = value;
  }

  getDateTo(): Date {
    return this.dateTo;
  }

  reserve(): void {
    let customerId = this.customerService.getLoggedCustomerId(),
      hasWifi = false,
      hasGPS = false,
      hasChildSeat = false,
      isAccepted = false,
      carId = this.car.id,
      dateFrom = this.dateFrom,
      dateTo = this.dateTo,
      carServicePeriod = new Date(this.car.servicePeriod);
    if (this.rentalService.validateRange(dateTo, dateFrom)
        && this.rentalService.validateServiceDate(dateTo, dateFrom, carServicePeriod)
        && this.rentalService.validateReservedDates(this.rentals, carId, dateTo, dateFrom)) {
      if (!this.dateFrom && !this.dateTo) { return; }
      this.rentalService.addRental({ customerId, carId, dateFrom, dateTo, hasWifi, hasGPS, hasChildSeat, isAccepted } as Rental)
        .subscribe(rental => {
          this.rentals.push(rental);
        });
      this.customers[customerId - 1].rentCars.push(carId);
      this.customerService.updateCustomer(this.customers[customerId - 1])
        .subscribe();

      alert("Congratulations! Request sent to admin");
      this.goBack();
    }
  }
}
