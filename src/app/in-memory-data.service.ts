import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const cars = [
      {
        id: 1,
        mark: 'Renault',
        model: 'Clio',
        description: 'Compact car',
        color: 'red',
        productionDate: new Date(2010, 9, 12),
        retirementDate: new Date(2020, 9, 12),
        mileage: 1234356,
        servicePeriod: new Date(2018, 4, 4)
      },
      {
        id: 2,
        mark: 'Seat',
        model: 'Leon',
        description: 'Compact car',
        color: 'white',
        productionDate: new Date(2016, 10, 20),
        retirementDate: new Date(2026, 10, 20),
        mileage: 50643,
        servicePeriod: new Date(2018, 5, 10)
      },
      {
        id: 3,
        mark: 'Alfa Romeo',
        model: 'Giulietta',
        description: 'Compact car',
        color: 'white',
        productionDate: new Date(2013, 4, 15),
        retirementDate: new Date(2023, 4, 15),
        mileage: 100412,
        servicePeriod: new Date(2018, 6, 10)
      },
      {
        id: 4,
        mark: 'Renault',
        model: 'Captur',
        description: 'Crossover',
        color: 'gray',
        productionDate: new Date(2015, 5, 10),
        retirementDate: new Date(2025, 5, 10),
        mileage: 74865,
        servicePeriod: new Date(2018, 7, 14)
      },
      {
        id: 5,
        mark: 'Toyota',
        model: 'Auris',
        description: 'Compact car',
        color: 'blue',
        productionDate: new Date(2017, 8, 24),
        retirementDate: new Date(2027, 8, 24),
        mileage: 40312,
        servicePeriod: new Date(2018, 8, 24)
      },
      {
        id: 6,
        mark: 'Ford',
        model: 'Explorer',
        description: 'SUV',
        color: 'black',
        productionDate: new Date(2014, 9, 26),
        retirementDate: new Date(2024, 9, 26),
        mileage: 220527,
        servicePeriod: new Date(2018, 6, 21)
      },
      {
        id: 7,
        mark: 'Chevrolet',
        model: 'Corvette',
        description: 'Convertible',
        color: 'red',
        productionDate: new Date(2018, 0, 30),
        retirementDate: new Date(2028, 0, 30),
        mileage: 10376,
        servicePeriod: new Date(2019, 0, 30)
      },
      {
        id: 8,
        mark: 'Mazda',
        model: 'RX8',
        description: 'Coupe',
        color: 'black',
        productionDate: new Date(2009, 7, 17),
        retirementDate: new Date(2019, 7, 17),
        mileage: 120412,
        servicePeriod: new Date(2018, 7, 17)
      },
      {
        id: 9,
        mark: 'Fiat',
        model: '500',
        description: 'City car',
        color: 'green',
        productionDate: new Date(2016, 11, 6),
        retirementDate: new Date(2026, 11, 6),
        mileage: 93563,
        servicePeriod: new Date(2018, 10, 19)
      },
      {
        id: 10,
        mark: 'Volkswagen',
        model: 'Golf',
        description: 'Compact car',
        color: 'red',
        productionDate: new Date(2015, 8, 12),
        retirementDate: new Date(2025, 8, 12),
        mileage: 151516,
        servicePeriod: new Date(2018, 8, 15)
      },
      {
        id: 11,
        mark: 'Ford',
        model: 'Mustang',
        description: 'Convertible',
        color: 'yellow',
        productionDate: new Date(2017, 10, 15),
        retirementDate: new Date(2027, 10, 15),
        mileage: 40002,
        servicePeriod: new Date(2018, 10, 15)
      }
    ];

    const customers = [
      {
        id: 1,
        name: 'Antek',
        surname: 'Smykiewicz',
        email: 'asmykiewicz@gmail.com',
        login: 'customer1',
        password: 'customer1',
        DOB: new Date(1991, 5, 21),
        rentCars: [2]
      },
      {
        id: 2,
        name: 'Janusz',
        surname: 'Tracz',
        email: 'jtrancz@gmail.com',
        login: 'customer2',
        password: 'customer2',
        DOB: new Date(1987, 3, 14),
        rentCars: []
      },
      {
        id: 3,
        name: 'Arnold',
        surname: 'Nowy',
        email: 'anowy@gmail.com',
        login: 'customer3',
        password: 'customer3',
        DOB: new Date(1976, 1, 18),
        rentCars: [6]
      },
      {
        id: 4,
        name: 'Jaroslaw',
        surname: 'Pempera',
        email: 'jpempera@gmail.com',
        login: 'customer4',
        password: 'customer4',
        DOB: new Date(1983, 6, 31),
        rentCars: [3]
      },
    ];

    const rentals = [
      {
        id: 1,
        customerId: 1,
        carId: 2,
        dateFrom: new Date(2018, 3, 6),
        dateTo: new Date(2018, 3, 9),
        hasWifi: false,
        hasGPS: true,
        hasChildSeat: false,
        isAccepted: true
      },
      {
        id: 2,
        customerId: 3,
        carId: 6,
        dateFrom: new Date(2018, 3, 8),
        dateTo: new Date(2018, 3, 12),
        hasWifi: true,
        hasGPS: true,
        hasChildSeat: false,
        isAccepted: true
      },
      {
        id: 3,
        customerId: 4,
        carId: 3,
        dateFrom: new Date(2018, 3, 16),
        dateTo: new Date(2018, 3, 18),
        hasWifi: true,
        hasGPS: true,
        hasChildSeat: false,
        isAccepted: false
      },
    ];

    return { cars, customers, rentals };
  }
}