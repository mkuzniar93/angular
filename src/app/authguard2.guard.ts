import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdministratorService } from './administrator.service';

@Injectable()
export class Authguard2Guard implements CanActivate {

  constructor(private administrator: AdministratorService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.administrator.getAdministratorLoggedIn();
  }
}
