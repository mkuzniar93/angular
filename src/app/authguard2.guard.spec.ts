import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Authguard2Guard } from './authguard2.guard';
import { AdministratorService } from './administrator.service';

describe('Authguard2Guard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Authguard2Guard, AdministratorService]
    });
  });

  it('should ...', inject([Authguard2Guard], (guard: Authguard2Guard) => {
    expect(guard).toBeTruthy();
  }));
});
