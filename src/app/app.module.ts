import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';


import { AppComponent } from './app.component';
import { CarsComponent } from './cars/cars.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { CarService } from './car.service';
import { CustomerService } from './customer.service';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CarSearchComponent } from './car-search/car-search.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AuthguardGuard } from './authguard.guard';
import { AdministratorService } from './administrator.service';
import { Authguard2Guard } from './authguard2.guard';
import { CustomersComponent } from './customers/customers.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerSearchComponent } from './customer-search/customer-search.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SortingCarsPipe } from './sorting-cars.pipe';
import { FilterPipe } from './filter.pipe';
import { SortingCustomersPipe } from './sorting-customers.pipe';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { ReservationComponent } from './reservation/reservation.component';
import { RentalService } from './rental.service';
import { RentalsComponent } from './rentals/rentals.component';
import { RentalDetailComponent } from './rental-detail/rental-detail.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';


@NgModule({
  declarations: [
    AppComponent,
    CarsComponent,
    CarDetailComponent,
    DashboardComponent,
    CarSearchComponent,
    LoginFormComponent,
    CustomersComponent,
    CustomerDetailComponent,
    CustomerSearchComponent,
    SortingCarsPipe,
    FilterPipe,
    SortingCustomersPipe,
    CustomerDashboardComponent,
    ReservationComponent,
    RentalsComponent,
    RentalDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false })
  ],
  providers: [
    CarService,
    CustomerService,
    AdministratorService,
    AuthguardGuard,
    Authguard2Guard,
    RentalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
