export class Car {
    id: number;
    mark: string;
    model: string;
    description: string;
    remarks: string;
    color: string;
    productionDate: Date;
    retirementDate: Date;
    mileage: number;
    servicePeriod: Date;
  }